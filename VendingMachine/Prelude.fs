﻿[<AutoOpen>]
module Prelude

let Ok x = Choice1Of2 x
let Fail x = Choice2Of2 x

let (|Ok|Fail|) = function
    | Choice1Of2 x -> Ok x
    | Choice2Of2 err -> Fail err