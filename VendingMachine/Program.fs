﻿open System
open ExtCore.Control.Collections
open ExtCore.Control
open Bag
open Machine
open System.Text.RegularExpressions

let productName =
    function
    | Coffee -> "Coffee"
    | WhiteCoffee -> "WhiteCoffee"
    | Tea -> "Tea"
    | Juice -> "Juice"

let withColor color todo =
    Console.ForegroundColor <- color
    try todo () finally Console.ResetColor()

let warn todo = withColor ConsoleColor.Yellow todo
let error = withColor ConsoleColor.Red
let success = withColor ConsoleColor.Green
let accent = withColor ConsoleColor.Cyan
let info = withColor ConsoleColor.White

let printMachine machine =
    accent <| fun _ ->
        printfn "========================================"
        printfn "Machine: %A" machine.Wallet
        machine.Wallet
        |> Map.fold (fun s coin count -> s + (coinRate coin * count)) 0u
        |> printfn "Total: %d"
        printfn "Credit: %d" machine.Credit
        printfn "========================================"

    info <| fun _ ->
        machine.Resource
        |> Map.toSeq
        |> Seq.iter
               (function
               | product, 0u -> printfn "%12s (--): out of items" (productName product)
               | product, count when productPrice product > machine.Credit ->
                   printfn "%12s (%2d): costs %3d rub" (productName product) count (productPrice product)
               | product, count ->
                   printfn "%12s (%2d): costs %3d rub; can be purchased by command: buy %A" (productName product) count
                       (productPrice product) product)

let printAvailableCommands _ =
    printfn "q | quit           -- quit from program"
    printfn "insert [1/2/5/10]  -- insert coin"
    printfn "return             -- return money from coin receiver (optimized)"
    printfn "buy <product>      -- purchase product"

let (|Insert|_|) row =
    match Regex.Match(row, "insert\s+(?<amount>\d+)") with
    | m when m.Success ->
        match System.UInt32.TryParse(m.Groups.["amount"].Value) with
        | true, 10u -> Some Of10
        | true, 5u -> Some Of5
        | true, 2u -> Some Of2
        | true, 1u -> Some Of1
        | _, _ -> None
    | _ -> None

let (|Buy|_|) row =
    match Regex.Match(row, "buy\s+(?<product>\w+)") with
    | m when m.Success ->
        match m.Groups.["product"].Value |> String.toLower with
        | "tea" -> Some Tea
        | "coffee" -> Some Coffee
        | "whitecoffee" -> Some WhiteCoffee
        | "juice" -> Some Juice
        | _ -> None
    | _ -> None

let (|ReturnMoney|_|) row =
    match row |> String.toLower with
    | "return" -> Some()
    | _ -> None

[<EntryPoint>]
let main _ =
    let resource =
        map [ (Tea, 10u)
              (Coffee, 20u)
              (WhiteCoffee, 20u)
              (Juice, 15u) ]

    let wallet =
        map [ (Of1, 100u)
              (Of2, 100u)
              (Of5, 100u)
              (Of10, 100u) ]

    let machine = Machine.createMachine resource wallet

    let rec loop() =
        let state = machine.PostAndReply GetState
        printMachine state
        success <| fun _ ->
            printf "$ "
        match Console.ReadLine() with
        | "quit" | "q" -> ()
        | s ->
            match s with
            | Insert coin -> machine.Post <| InsertCoin coin
            | ReturnMoney() ->
                success <| fun _ ->
                    machine.PostAndReply ReturnMoney |> printfn "Money returned, change: %A"
            | Buy product ->
                match machine.PostAndReply(fun c -> Purchase(product, c)) with
                | Ok change ->
                    success <| fun _ ->
                        printfn "%s purchased; change: %A" (productName product) change
                | Fail err ->
                    error <| fun _ ->
                        printfn "Purchase error: %A" err
            | _ ->
                error <| fun _ ->
                    printAvailableCommands machine
            loop()
    loop()
    0