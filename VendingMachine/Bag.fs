﻿module Bag

open ExtCore.Control.Collections

type Count = uint32

type Bag<'t when 't : comparison> = Map<'t, Count>

let take whatToTake from =
    Map.keys from + Map.keys whatToTake
    |> List.ofSet
    |> Choice.List.map (fun x ->
           match from.TryFind x, whatToTake.TryFind x with
           | None, _ -> Fail ()
           | Some (inBag : uint32), None -> Ok(x, inBag)
           | Some inBag, Some toTake when inBag < toTake -> Fail ()
           | Some inBag, Some toTake -> Ok(x, inBag - toTake))
    |> Choice.toOption
    |> Option.map Map.ofSeq

let addItem x bag =
    match bag |> Map.tryFind x with
    | None -> bag.Add(x, 1u)
    | Some count -> bag.Add(x, count + 1u)

let add map1 map2 = Map.join (fun _ -> (+)) map1 map2

let rec exchangeOptions rate amount coins =
    seq {
        if amount = 0u then yield map []
        else
            match coins
                  |> List.sortWith (fun a b -> compare b a)
                  |> List.filter (fun x -> rate x <= amount) with
            | [] -> ()
            | coin :: restCoins ->
                let coinRate = rate coin
                let maxStepCoins = amount / coinRate
                for count in [ 1u..maxStepCoins ] |> List.rev do
                    yield! exchangeOptions rate (amount - count * coinRate) restCoins
                           |> Seq.map (add <| map [ (coin, count) ])
                yield! exchangeOptions rate amount restCoins
    }