﻿module Machine

open Bag
open ExtCore.Control

type Product =
    | Tea
    | Coffee
    | WhiteCoffee
    | Juice

let productPrice = function
    | Tea -> 13u
    | Coffee -> 18u
    | WhiteCoffee -> 21u
    | Juice -> 35u

type Coin =
    | Of1
    | Of2
    | Of5
    | Of10

let coinRate = function
    | Of1 -> 1u
    | Of2 -> 2u
    | Of5 -> 5u
    | Of10 -> 10u

let allCoins = [ Of1; Of2; Of5; Of10 ]

type MachineState =
    { Resource : Bag<Product>
      Wallet : Bag<Coin>
      Credit: uint32 }

type GetProductError =
    | OutOfProducts
    | NoChange
    | NotSufficientCredit

type Change = Bag<Coin>

type Command =
    | GetState of AsyncReplyChannel<MachineState>
    | Purchase of Product * AsyncReplyChannel<Choice<Change, GetProductError>>
    | InsertCoin of Coin
    | ReturnMoney of AsyncReplyChannel<Change>

let useCredit credit price =
    if credit >= price then Ok (credit - price)
    else Fail NotSufficientCredit

let findChange bag amount =
    Bag.exchangeOptions coinRate amount allCoins
    |> Seq.tryPick (fun change -> bag |> take change |> Option.map (fun wallet -> change, wallet))

let createMachine resource wallet =
    MailboxProcessor.Start(fun inbox ->
        let rec loop state =
            async {
                let! msg = inbox.Receive()
                let newState =
                    match msg with
                    | GetState reply ->
                        reply.Reply state
                        state
                    | InsertCoin coin ->
                        { state with Wallet = addItem coin state.Wallet
                                     Credit = state.Credit + coinRate coin }
                    | ReturnMoney reply ->
                        let change, wallet = findChange state.Wallet state.Credit |> Option.get
                        reply.Reply(change)
                        { state with Wallet = wallet
                                     Credit = 0u }
                    | Purchase(product, reply) ->
                        let result = choice {
                            let! leftProducts = take (map [(product, 1u)]) state.Resource |> Choice.ofOptionWith OutOfProducts
                            let! changeAmount = useCredit state.Credit (productPrice product)
                            let! change, wallet = findChange state.Wallet changeAmount |> Choice.ofOptionWith NoChange
                            return change, { state with Resource = leftProducts; Wallet = wallet; Credit = 0u }
                        }

                        match result with
                        | Ok (change, newState) ->
                            reply.Reply <| Ok change
                            newState
                        | Fail err ->
                            reply.Reply <| Fail err
                            state
                return! loop newState
            }
        loop { Resource = resource
               Wallet = wallet
               Credit = 0u })