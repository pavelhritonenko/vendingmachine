﻿module BagTests

open Bag
open Machine
open Program
open Swensen.Unquote
open NUnit.Framework

[<Test>]
let ``returns error when take from empty bag``() =
    map [ (Of1, 5u)
          (Of2, 2u) ]
    |> take (map [ (Of2, 3u) ])
    =? None

[<Test>]
let ``returns error when item that not in bag``() =
    map [ (Of1, 5u)
          (Of2, 2u) ]
    |> take (map [ (Of5, 3u) ])
    =? None

[<Test>]
let ``return bag without two items``() =
    map [ (Of1, 5u)
          (Of2, 2u) ]
    |> take (map [ (Of1, 3u) ])
    =? Some(map [ (Of1, 2u)
                  (Of2, 2u) ])

[<Test>]
let ``return error when particular item out of items``() =
    map [ (Of1, 5u)
          (Of2, 2u) ]
    |> take (map [ (Of1, 6u) ])
    =? None

let coins = [ Of1; Of2; Of5; Of10 ]

[<Test>]
let ``change 1`` () : unit =
    exchangeOptions Machine.coinRate 1u coins |> Seq.length =? 1

[<Test>]
let ``change 200`` () : unit =
    exchangeOptions Machine.coinRate 200u coins |> Seq.length  =? 15211